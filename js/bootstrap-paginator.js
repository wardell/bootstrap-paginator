/*!
 * Paginator Component for Twitter Bootstrap
 *
 * Copyright 2013 Joris de Wit
 *
 * Contributors https://github.com/jdewit/bootstrap-paginator/graphs/contributors
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
;(function($, window, document, undefined) {

  'use strict'; // jshint ;_;

  // TIMEPICKER PUBLIC CLASS DEFINITION
  var Paginator = function(container, options) {
    this.$container = $(container);
    this.template = options.template;

    this._init();
  };

  Paginator.prototype = {

    constructor: Paginator,

    _init: function() {
      var self = this;

    },

    next: function() {
    },

    previous: function() {
    },

    openPage: function(e) {
    },

    getPaginatorTemplate: function() {
      var template;


      template = '<div class="pagination">' +
				'<ul>' +
					'<li class="disabled"><a href="#">&laquo;</a></li>' + 
					'<li class="active"><a href="#">1</a></li>' +
					'<li><a href="#">»</a></li>' +
				'</ul>' 
			;

			return template;
    }
  };

  //PAGINATOR PLUGIN DEFINITION
  $.fn.paginator = function(option) {
    var args = Array.apply(null, arguments);
    args.shift();
    return this.each(function() {
      var $this = $(this),
        data = $this.data('paginator'),
        options = typeof option === 'object' && option;

      if (!data) {
        $this.data('paginator', (data = new Paginator(this, $.extend({}, $.fn.paginator.defaults, options, $(this).data()))));
      }

      if (typeof option === 'string') {
        data[option].apply(data, args);
      }
    });
  };

  $.fn.paginator.defaults = {
    template: 'pagination'
  };

  $.fn.paginator.Constructor = Paginator;

})(jQuery, window, document);
